export const lateCallsData = [
  {
    id: 'Late Calls',
    color: 'hsl(122, 70%, 50%)',
    data: [
      {
        x: '27/01',
        y: 11,
      },
      {
        x: '28/01',
        y: 21,
      },
      {
        x: '29/01',
        y: 13,
      },
      {
        x: '30/01',
        y: 8,
      },
      {
        x: '31/01',
        y: 4,
      },
      {
        x: '01/02',
        y: 13,
      },
      {
        x: '02/02',
        y: 9,
      },
      {
        x: '03/02',
        y: 1,
      },
      {
        x: '04/02',
        y: 8,
      },
      {
        x: '05/02',
        y: 10,
      },
    ],
  },
];

export const missedCallsData = [
  {
    date: '25/01',

    Missed: 86,
    MissedColor: 'hsl(45, 70%, 50%)',
    Incidents: 185,
    IncidentsColor: 'hsl(103, 70%, 50%)',
  },
  {
    date: '26/01',

    Missed: 5,
    MissedColor: 'hsl(118, 70%, 50%)',
    Incidents: 102,
    IncidentsColor: 'hsl(300, 70%, 50%)',
  },
  {
    date: '27/01',

    Missed: 119,
    MissedColor: 'hsl(73, 70%, 50%)',
    Incidents: 137,
    IncidentsColor: 'hsl(273, 70%, 50%)',
  },
  {
    date: '28/01',

    Missed: 122,
    MissedColor: 'hsl(247, 70%, 50%)',
    Incidents: 179,
    IncidentsColor: 'hsl(86, 70%, 50%)',
  },
  {
    date: '29/01',

    Missed: 40,
    MissedColor: 'hsl(77, 70%, 50%)',
    Incidents: 59,
    IncidentsColor: 'hsl(190, 70%, 50%)',
  },
  {
    date: '30/01',

    Missed: 69,
    MissedColor: 'hsl(234, 70%, 50%)',
    Incidents: 131,
    IncidentsColor: 'hsl(75, 70%, 50%)',
  },
  {
    date: '01/02',

    Missed: 109,
    MissedColor: 'hsl(135, 70%, 50%)',
    Incidents: 133,
    IncidentsColor: 'hsl(177, 70%, 50%)',
  },
];

export const missingNotesData = [
  {
    id: 'Mssing Notes',
    color: 'hsl(122, 70%, 50%)',
    data: [
      {
        x: '27/01',
        y: 11,
      },
      {
        x: '28/01',
        y: 21,
      },
      {
        x: '29/01',
        y: 13,
      },
      {
        x: '30/01',
        y: 8,
      },
      {
        x: '31/01',
        y: 4,
      },
      {
        x: '01/02',
        y: 13,
      },
      {
        x: '02/02',
        y: 9,
      },
      {
        x: '03/02',
        y: 1,
      },
      {
        x: '04/02',
        y: 8,
      },
      {
        x: '05/02',
        y: 10,
      },
    ],
  },
];

export const unassignedCallsData = [
  {
    id: 'Unassigned',
    color: 'hsl(122, 70%, 50%)',
    data: [
      {
        x: '27/01',
        y: 11,
      },
      {
        x: '28/01',
        y: 21,
      },
      {
        x: '29/01',
        y: 13,
      },
      {
        x: '30/01',
        y: 8,
      },
      {
        x: '31/01',
        y: 4,
      },
      {
        x: '01/02',
        y: 13,
      },
      {
        x: '02/02',
        y: 9,
      },
      {
        x: '03/02',
        y: 1,
      },
      {
        x: '04/02',
        y: 8,
      },
      {
        x: '05/02',
        y: 10,
      },
    ],
  },
];
